## License

Copyright (c) 2020 Vikaskumar Patel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this application and associated documentation files (the "Foodyfi"), to deal
in the application without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the application, and to permit persons to whom the application is
furnished to do.
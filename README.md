# Foodyfi

## Introduction

The problem of unprecedented COVID-19 effect on the Canadian Restaurant Industry, impacted in a way which has led to shutdown of many restaurants due to lockdown. As people must follow social distancing and it is very difficult to serve customers in a restaurant by following the health and safety protocols now and using a third party to deliver food reduces profit.

This can be solved with a food ordering application which will have a function to order food and payment for the customers so that they do not have to worry about the social distancing and the food will be delivered to them at their home with contact less delivery.
We have incorporated this proposition which would assist even small business to increase the safety, efficiency, and productivity in this situation. This application will help restaurant to receive orders and manage them and it will also decrease the cost of the operations for the restaurant because when the restaurant uses a third-party food ordering system it increases the cost of the operations due to service fees.

---

## Technology Listing:

* Node.js
* MongoDB
* HTML
* CSS




---

## Prerequisites

* Visual Studio Code - https://code.visualstudio.com/download
* Node.js - https://nodejs.org/en/download/
* MongoDB - https://www.mongodb.com/try/download/community

---

## Setup

### Install Dependency 

```
npm install
```

### Start Server

```
npm server
```

### Create a package.json file

```
npm init
```

### Run Application

```
node index.ejs
```

### Access Web Browser

```
http://localhost:3000/
```

---

## License

Copyright (c) 2020 Vikaskumar Patel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this application and associated documentation files (the "Foodyfi"), to deal
in the application without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the application, and to permit persons to whom the application is
furnished to do.
